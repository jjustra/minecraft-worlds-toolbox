# MineCraft worlds toolbox

Set of tools to :

- list versions

of minecraft worlds (circa 1.9+ - more precisely 15w32a [Release date : August 5, 2015]).


## What's in it

- **bingrep** is python script for more reliable grepping of binary files

- **unpack** is python script that takes binary input and unpacks it to its textual representation (e.g.: printf '\x00\x00\x00\x20' | unpack '>i')

- **version-minecraft-world** unpacks version of world from level.dat file or tries to guess it from time world was last played

- **ls-minecraft-worlds** should be run in directory with all your worlds and will print their versions

