install:
	cp bingrep /usr/local/bin/
	chmod +x /usr/local/bin/bingrep
	cp unpack /usr/local/bin/
	chmod +x /usr/local/bin/unpack
	cp version-minecraft-world /usr/local/bin/
	chmod +x /usr/local/bin/version-minecraft-world
	cp ls-minecraft-worlds /usr/local/bin/
	chmod +x /usr/local/bin/ls-minecraft-worlds
