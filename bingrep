#!/bin/env python

# usage : bingrep [-rnq] "pattern to match" < stdin_input
#  -r - print text representation of matched binary to output
#  -n - new line after each result
#  -q - quiet, print nothing to output
# exit code is 0 on match, 1 on not match

import sys


_t = 0  # Text output
_n = '' # Output separator
_q = 0  # Quiet
pat_l = []
exit_code = 1

# Get me some binary source
if hasattr(sys.stdin, 'buffer'):
	# Python 3
	f = sys.stdin.buffer
else:
	# Python 2
	f = sys.stdin


# Parse args
for a in sys.argv[1:]:
	if not a: continue

	if a[0] == '-':
		# Parse regular args

		for c in a[1:]:
			if c == 'q': _q = 1

	else:
		# Parse search pattern

		esc = 0
		esc_x = 0
		x = ''
		search = []

		for c in a:
			if esc == 1:
				if esc_x:
					# Parse hex escape code

					x += c

					if len(x) == 2:
						# End hex parsing
						c = int(x, 16)
						esc_x = 0
						esc = 0
					else:
						continue

				else:

					# Translate (or start hex parsing) if needed

					if c == '0': c = char(0)
					elif c == 'n': c = '\n'
					elif c == 'r': c = '\r'
					elif c == 't': c = '\t'
					elif c == 's': c = ' '
					elif c == 'x':
						# Start hex parsing
						x = ''
						esc_x = 1
						continue

				search.append(c)
				esc = 0
				continue

			if c == '\\':
				esc = 1
				continue

			if c == '.':
				# Any byte
				c = -1

			search.append(c)

		if search:
			pat_l.append({
				'pat': search,
				'pos': -1, # how far in pattern are we matching
				'match': ''
			})

# Do the search
while 1:
	c = f.read(1)
	if not c: break

	for pat in pat_l:
		if pat['pos'] + 1 < len(pat['pat']):
			# We can keep/start matching

			_c = pat['pat'][pat['pos'] + 1]
			if _c == -1 or _c == c:
				# Match
				pat['match'] += c
				pat['pos'] += 1
			else:
				# Not match
				pat['match'] = ''
				pat['pos'] = -1

		else:
			# Matching ended

			if not _q:
				if _t:
					sys.stdout.write(repr(pat['match']) + _n)
				else:
					sys.stdout.write(pat['match'] + _n)

			pat['match'] = ''
			pat['pos'] = -1
			exit_code = 0

sys.exit(exit_code)